<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'message' => $faker->text(100),
        'user_id' => App\User::all()->random()->id
    ];
});

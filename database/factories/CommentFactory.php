<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'comment' => $faker->text(100),
        'user_id' => App\User::all()->random()->id,
        'post_id' => App\Post::all()->random()->id
    ];

});



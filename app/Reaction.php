<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    public $fillable = ['type_reaction_id', 'post_id', 'user_id'];

    public function TypeReation() {
        return $this->belongsTo('App\TypeReaction');
    }

    public function Post() {
        return $this->belongsTo('App\Post');
    }

    public function User() {
        return $this->belongsTo('App\User');
    }
}

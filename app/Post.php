<?php

namespace App;

use App\Power;
use App\TypeReaction;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $fillable = ['message','picture','user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function comments() {
         return $this->hasMany('App\Comment');
     }

    public function reaction() {
         return $this->hasMany('App\Reaction');
     }
      
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeReaction extends Model
{
    public $fillable = ['label',];

    public function reaction() {
        return $this->belongsTo(Reaction::class);
    }
}

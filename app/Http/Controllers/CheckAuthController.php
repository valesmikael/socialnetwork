<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckAuthController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
}